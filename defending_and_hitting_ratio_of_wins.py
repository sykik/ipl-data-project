import matplotlib.pyplot as plt


def defending_or_hitting_winner(matches):
    """compute the defending or hitting ratio from total winnings"""
    defending_hitting_winners = {}
    for match in matches:
        if match['winner'] not in defending_hitting_winners and match['win_by_runs'] != '0':
            defending_hitting_winners[match['winner']] = {
                'Defending': 1, 'Hitting': 0}
        elif match['winner'] in defending_hitting_winners and match['win_by_runs'] != '0':
            defending_hitting_winners[match['winner']]['Defending'] += 1
        elif match['winner'] not in defending_hitting_winners and match['win_by_wickets'] != '0':
            defending_hitting_winners[match['winner']] = {
                'Defending': 0, 'Hitting': 1}
        elif match['winner'] in defending_hitting_winners and match['win_by_wickets'] != '0':
            defending_hitting_winners[match['winner']]['Hitting'] += 1
    # because in our data we have 2 entries for 'Rising Pune Supergiants'
    for team in defending_hitting_winners:
        temp = defending_hitting_winners[team]
        if team == 'Rising Pune Supergiants':
            defending_hitting_winners['Rising Pune Supergiant']['Hitting'] += temp['Hitting']
            defending_hitting_winners['Rising Pune Supergiant']['Defending'] += temp['Defending']
    del defending_hitting_winners['Rising Pune Supergiants']
    return defending_hitting_winners


def plot_defending_or_hitting_winner(defending_hitting_winners):
    """compute the defending or hitting ratio from total winnings"""
    plt.subplots(figsize=(10, 10))
    team_initials = create_initials(defending_hitting_winners)
    hitting = [defending_hitting_winners[team]['Hitting']
               for team in defending_hitting_winners]
    defending = [defending_hitting_winners[team]['Defending']
                 for team in defending_hitting_winners]
    plt.bar(team_initials, hitting)
    plt.bar(team_initials, defending, bottom=hitting)
    plt.legend(['Hitting', 'Defending'])
    plt.xlabel('Teams')
    plt.ylabel('Total Wins')
    plt.title('Defending & Hitting Ratio')
    plt.savefig('defending_hitting_ratio.png')
    plt.show()


def create_initials(teams):
    """function to return team initials"""
    initial_of_teams = []
    for team in teams:
        initials = ''
        temp = team.split()
        for part in temp:
            initials += part[0]
        initial_of_teams.append(initials)
    return initial_of_teams


def compute_and_plot_defending_hitting_winners(matches):
    """calling the compute and plot functions"""
    defending_hitting_winners = defending_or_hitting_winner(matches)
    plot_defending_or_hitting_winner(defending_hitting_winners)
