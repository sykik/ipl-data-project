## Description
In this data assignment you will transform raw data from `IPL` into graphs that will convey some meaning / analysis. For each part of this assignment you will have 2 parts -

`Download` both csv files from https://www.kaggle.com/manasgarg/ipl

#### Part 1 - `Python`:

Code python functions that will transform the raw csv data into a data structure in a format suitable for plotting with matplotlib.

`Generate the following plots` -

1. Plot the number of matches played per year of all the years in IPL.
2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
3. For the year 2016 plot the extra runs conceded per team.
4. For the year 2015 plot the top economical bowlers.
5. Discuss a "Story" you want to tell with the given data. As with part 1, prepare the data structure and plot with matplotlib.


#### Part 2 - `Unit Testing`:

`Rewrite` the data project. Create your own smaller dataset - like 5 matches and 15 deliveries. Manually set the result for the unit tests.

#### Part 3- `SQL`:

Import your test source into a Postgresql database i.e. ipl_test_db. Write SQL queries for each of the above questions. Save it into a text file. Use the psql prompt or pgadmin to write queries.

If you want to use `psql`, become familiar with `\l, \c, \dt, \d+, \dd` commands.


1. Now, connect to the database from Python using the `psycopg2` library
2. Define separate functions which execute each query.
3. These functions must return the same data as your Python business logic functions, so that the corresponding plot functions can be shared between them.
4. Compare the SQL output with your original Python functions' output.

### Usage:

Directory structure:

                                    IPL Data Project
                                    ├── tests
                                    │   ├── mock_deliveries.csv
                                    │   ├── mock_matches.csv
                                    │   ├── test_defending_and_hitting_ratio_of_wins.py
                                    │   ├── test_extra_runs_conceded.py
                                    │   ├── test_matches_played_per_year.py
                                    │   ├── test_no_of_matches_won.py
                                    │   ├── test_sql.py
                                    │   └── test_top_economic_bowler.py
                                    ├── configuration.ini
                                    ├── defending_and_hitting_ratio_of_wins.py
                                    ├── defending_hitting_ratio.png
                                    ├── deliveries.csv
                                    ├── extra_runs_conceded.png
                                    ├── extra_runs_conceded.py
                                    ├── main.py
                                    ├── matches.csv
                                    ├── matches_played_per_year.png
                                    ├── matches_played_per_year.py
                                    ├── no_of_matches_won.png
                                    ├── no_of_matches_won.py
                                    ├── README.md
                                    ├── requirements.txt
                                    ├── sql.py
                                    ├── sql.txt
                                    ├── top_economic_bowler.png
                                    └── top_economic_bowler.py
Instructions:

1. To run and plot the problems execute the `main.py`
2. To run tests for the files goto `tests` folder and execute the files with prefix `test_` followed by the file name,
    or you can write the following command in terminal `pytest -m unittest` in `tests` folder.
3. To execute sql quaries call functions in `sql.py` file.
4. Before executing the `test` for `SQL` create `mock tables` for `deliveries` and `matches` from `mock_matches.csv` and `mock_deliveries.csv`
    from the `tests` folder.