import matplotlib.pyplot as plt


def extra_runs_conceded(matches, deliveries, year):
    """compute the extra runs conceded in specified year"""
    extras_by_team = {}
    for match in matches:
        if year in match['season']:
            for delivery in deliveries:
                extra_run = int(delivery['extra_runs'])
                if delivery['match_id'] == match['id'] and delivery['is_super_over'] == '0':
                    if delivery['bowling_team'] not in extras_by_team:
                        extras_by_team[delivery['bowling_team']] = extra_run
                    else:
                        extras_by_team[delivery['bowling_team']] += extra_run
    return extras_by_team


def plot_extra_runs_conceded(extras_conceded):
    """plot the extra runs conceded in specified year"""
    plt.subplots(figsize=(10, 10))
    plt.bar(*zip(*sorted(extras_conceded.items())),color='purple')
    plt.xlabel('Teams')
    plt.ylabel('Runs')
    plt.xticks(rotation=65)
    plt.title('Extras (2016)')
    plt.savefig('extra_runs_conceded.png')
    plt.show()


def compute_and_plot_extra_runs_conceded(matches, deliveries, year):
    """calling the compute and plot functions"""
    extras_conceded = extra_runs_conceded(matches, deliveries, year)
    plot_extra_runs_conceded(extras_conceded)
