from os import path
import sys
import unittest
sys.path.append(path.join(path.dirname(__file__), '..')) #adding the path of parent directory to sys.path
from no_of_matches_won import no_of_matches_won
from no_of_matches_won import return_teams
from no_of_matches_won import return_years
from no_of_matches_won import create_initials
from main import extract_matches






class TestNoOfMatchesWon(unittest.TestCase):

    def test_no_of_matches_won(self):
        mock_matches = extract_matches('mock_matches.csv')
        output,teams = no_of_matches_won(mock_matches)
        expected_output = {'Sunrisers Hyderabad': {'2008': 0, '2009': 0, '2010': 0, '2011': 0, '2015': 0, '2016': 0, '2017': 1},
                            'Kolkata Knight Riders': {'2008': 1, '2009': 0, '2010': 1, '2011': 0, '2015': 1, '2016': 1, '2017': 0},
                            'Mumbai Indians': {'2008': 0, '2009': 1, '2010': 0, '2011': 0, '2015': 0, '2016': 0, '2017': 0},
                            'Chennai Super Kings': {'2008': 0, '2009': 0, '2010': 0, '2011': 1, '2015': 1, '2016': 0, '2017': 0},
                            'Rajasthan Royals': {'2008': 0, '2009': 0, '2010': 0, '2011': 1, '2015': 0, '2016': 0, '2017': 0},
                            'Rising Pune Supergiant': {'2008': 0, '2009': 0, '2010': 0, '2011': 0, '2015': 0, '2016': 2, '2017': 0}}
        expected_teams = ['Sunrisers Hyderabad',
                        'Kolkata Knight Riders',
                        'Mumbai Indians',
                        'Chennai Super Kings',
                        'Rajasthan Royals',
                        'Rising Pune Supergiant']
        self.assertEqual(output, expected_output)
        self.assertEqual(teams,expected_teams)

    def test_return_teams(self):
        mock_matches = extract_matches('mock_matches.csv')
        output = return_teams(mock_matches)
        expected_teams = ['Sunrisers Hyderabad',
                        'Kolkata Knight Riders',
                        'Mumbai Indians',
                        'Chennai Super Kings',
                        'Rajasthan Royals',
                        'Rising Pune Supergiant',
                        'Rising Pune Supergiants']
        self.assertEqual(output,expected_teams)

    def test_return_years(self):
        mock_matches = extract_matches('mock_matches.csv')
        output = return_years(mock_matches)
        expected_years = ['2008','2009','2010','2011','2015','2016','2017']
        self.assertEqual(output,expected_years)

    def test_create_initials(self):
        mock_matches = extract_matches('mock_matches.csv')
        teams = return_teams(mock_matches)
        output = create_initials(teams)
        expected_initials = ['SH','KKR','MI','CSK','RR','RPS','RPS']
        self.assertEqual(output,expected_initials)


if __name__ == '__main__':
    unittest.main()