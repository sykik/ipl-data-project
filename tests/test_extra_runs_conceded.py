from os import path
import sys
import unittest
sys.path.append(path.join(path.dirname(__file__), '..')) #adding the path of parent directory to sys.path
from extra_runs_conceded import extra_runs_conceded
from main import extract_matches
from main import extract_deliveries


class TestExtraRunsConceded(unittest.TestCase):

    def test_matches_played_per_year(self):
        mock_matches = extract_matches('mock_matches.csv')
        mock_deliveries = extract_deliveries('mock_deliveries.csv')
        year = '2008'
        output = extra_runs_conceded(mock_matches,mock_deliveries,year)
        expected_output = {'Royal Challengers Bangalore': 3}
        self.assertEqual(output, expected_output)
        year = '2011'
        output = extra_runs_conceded(mock_matches,mock_deliveries,year)
        expected_output = {'Kolkata Knight Riders': 0, 'Rajasthan Royals': 0}
        self.assertEqual(output,expected_output)
        year = '2016'
        output = extra_runs_conceded(mock_matches,mock_deliveries,year)
        expected_output = {'Kolkata Knight Riders': 1,
                            'Rising Pune Supergiant': 8,
                            'Rising Pune Supergiants': 8}
        self.assertEqual(output, expected_output)



if __name__ == '__main__':
    unittest.main()