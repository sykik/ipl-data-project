from os import path
import sys
import unittest
sys.path.append(path.join(path.dirname(__file__), '..')) #adding the path of parent directory to sys.path
from sql import execute_query
from sql import matches_played_per_year
from sql import no_of_matches_won
from sql import extra_runs_conceded
from sql import top_economic_bowler
from sql import defending_hitting_ratio


class TestSql(unittest.TestCase):

    def test_matches_played_per_year(self):
        result = matches_played_per_year(test=True)
        expected_result = {2008: 1,
                        2009: 1,
                        2010: 1,
                        2011: 2,
                        2015: 2,
                        2016: 3,
                        2017: 1}
        self.assertEqual(result,expected_result)

    def test_no_of_matches_won(self):
        result = no_of_matches_won(test=True)
        expected_result = {'Chennai Super Kings': {2011: 1, 2015: 1, 2016: 0, 2017: 0, 2008: 0, 2009: 0, 2010: 0},
                        'Kolkata Knight Riders': {2008: 1, 2010: 1, 2015: 1, 2016: 1, 2017: 0, 2009: 0, 2011: 0},
                        'Mumbai Indians': {2009: 1, 2016: 0, 2017: 0, 2008: 0, 2010: 0, 2011: 0, 2015: 0},
                        'Rajasthan Royals': {2011: 1, 2016: 0, 2017: 0, 2008: 0, 2009: 0, 2010: 0, 2015: 0},
                        'Rising Pune Supergiant': {2016: 1, 2017: 0, 2008: 0, 2009: 0, 2010: 0, 2011: 0, 2015: 0},
                        'Sunrisers Hyderabad': {2017: 1, 2016: 0, 2008: 0, 2009: 0, 2010: 0, 2011: 0, 2015: 0}}
        self.assertEqual(result,expected_result)

    def test_extra_runs_conceded(self):
        result = extra_runs_conceded(2008,test=True)
        expected_result = {'Royal Challengers Bangalore': 3}
        self.assertEqual(result,expected_result)
        result = extra_runs_conceded(2010,test=True)
        expected_result = {'Deccan Chargers': 5}
        self.assertEqual(result,expected_result)
        result = extra_runs_conceded(2015,test=True)
        expected_result = {'Delhi Daredevils': 10,
                        'Kolkata Knight Riders': 1}
        self.assertEqual(result,expected_result)

    def test_top_economic_bowler(self):
        result = top_economic_bowler(2008,test=True)
        expected_result = {'P Kumar': 0.75,
                        'Z Khan': 18.0}
        self.assertEqual(result,expected_result)
        result = top_economic_bowler(2017,test=True)
        expected_result = {'A Choudhary': 16.0,
                        'TS Mills': 6.0}
        self.assertEqual(result,expected_result)
        result = top_economic_bowler(2013,test=True)
        expected_result = {}
        self.assertEqual(result,expected_result)

    def test_defending_hitting_ratio(self):
        result = defending_hitting_ratio(test=True)
        expected_result = {'Chennai Super Kings': {'Defending': 0, 'Hitting': 2, 'Total Wins': 2},
                        'Kolkata Knight Riders': {'Defending': 2, 'Hitting': 2, 'Total Wins': 4},
                        'Mumbai Indians': {'Defending': 0, 'Hitting': 1, 'Total Wins': 1},
                        'Rajasthan Royals': {'Defending': 1, 'Hitting': 0, 'Total Wins': 1},
                        'Rising Pune Supergiants': {'Defending': 2, 'Hitting': 0, 'Total Wins': 2},
                        'Sunrisers Hyderabad': {'Defending': 0, 'Hitting': 1, 'Total Wins': 1}}
        self.assertEqual(result,expected_result)



if __name__ == "__main__":
    unittest.main()