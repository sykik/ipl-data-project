from os import path
import sys
import unittest
sys.path.append(path.join(path.dirname(__file__), '..')) #adding the path of parent directory to sys.path
from matches_played_per_year import matches_played_per_year
from main import extract_matches


class TestMatchesPlayedPerYear(unittest.TestCase):

    def test_matches_played_per_year(self):
        mock_matches = extract_matches('mock_matches.csv')
        output = matches_played_per_year(mock_matches)
        expected_output = {'2017': 1, '2008': 1, '2009': 1, '2010': 1, '2011': 2, '2016': 3, '2015': 2}
        self.assertEqual(output, expected_output)


if __name__ == '__main__':
    unittest.main()