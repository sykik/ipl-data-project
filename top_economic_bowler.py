import matplotlib.pyplot as plt


def economy_of_bowlers(matches, deliveries, year, cutoff=6):
    """
    Function to calculate top economic bowlers,
    cutoff is set to 1 over, i.e. 6 balls
    """
    bowlers_economy, bowlers_economy_rate = {}, {}
    for match in matches:
        if year == match['season']:
            for delivery in deliveries:
                if delivery['match_id'] == match['id'] and delivery['is_super_over'] == '0':
                    total, bye, legbye, name = int(
                        delivery['total_runs']), int(
                        delivery['bye_runs']), int(
                        delivery['legbye_runs']), delivery['bowler']
                    if name not in bowlers_economy:
                        bowlers_economy[name] = {
                            'Total Runs': total - (bye + legbye), 'No of Balls': 0}
                        if delivery['wide_runs'] == '0' and delivery['noball_runs'] == '0':
                            bowlers_economy[name]['No of Balls'] += 1
                    else:
                        bowlers_economy[name]['Total Runs'] += total - \
                            (bye + legbye)
                        if delivery['wide_runs'] == '0' and delivery['noball_runs'] == '0':
                            bowlers_economy[name]['No of Balls'] += 1
    for bowler in bowlers_economy:
        # filtering the players who has bowled for more than 10 overs
        if bowlers_economy[bowler]['No of Balls'] >= cutoff:  #i have saved plot of cutoff = 12 i.e. 2 overs
            bowlers_economy_rate[bowler] = round(
                ((bowlers_economy[bowler]['Total Runs']) *
                 6) /
                bowlers_economy[bowler]['No of Balls'],
                2)
    print(bowlers_economy_rate)
    return bowlers_economy_rate


def plot_economy_of_bowlers(economy_rate):
    """plot the top 10 economic bowlers in specified year"""
    top_economic_bowlers, economies = get_top_economic_bowlers(economy_rate)
    plt.subplots(figsize=(10, 10))
    plt.bar(top_economic_bowlers, economies)
    plt.xlabel('Bowlers Name')
    plt.ylabel('Economy Rate')
    plt.title('Economy of Bowlers (2015)')
    plt.xticks(rotation=65)
    #plt.savefig('top_economic_bowler.png')
    plt.show()


def get_top_economic_bowlers(economy_rate):
    """function to compute the top 10 economic bowlers"""
    temp = dict(sorted({(value, key) for key, value in economy_rate.items()}))
    bowlers = [bowler for bowler in temp.values()]
    economies = [rate for rate in temp.keys()]
    return bowlers[:10], economies[:10]


def compute_and_plot_top_economic_bowlers(matches, deliveries, year):
    """calling the compute and plot functions"""
    economy_rate = economy_of_bowlers(matches, deliveries, year)
    plot_economy_of_bowlers(economy_rate)
